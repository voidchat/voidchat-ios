//  Created by Alex Gordon on 19/09/2018.
//  Copyright © 2018 Alex Gordon. See LICENSE.txt

import UIKit

extension UIViewController {
    func makeVC<T: UIViewController>(_ ident: String) -> T {
        return self.storyboard!.instantiateViewController(withIdentifier: ident) as! T
    }
}

class ServersTVC: UITableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        self.refreshLeftButton()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAction(_:)))
        
        let (lastUsed, servers, serversMap) = ServerListModel.shared.readState()
        if servers.isEmpty {
            // load initial view
            let nc: UINavigationController = self.makeVC("AddServerNC")
            let vc: AddServerVC = self.makeVC("AddServerVC")
            vc.firstLaunch = true
            vc.tvc = self
            nc.pushViewController(vc, animated: false)
            self.present(nc, animated: false, completion: nil)
        } else {
            if let lastUsed = lastUsed, let server = serversMap[lastUsed] {
                self.openServer(server)
            } else {
                // no selection
            }
            self.reload()
        }
    }
    
    var doneButton: UIBarButtonItem? = nil
    var editButton: UIBarButtonItem? = nil
    func refreshLeftButton() {
        if doneButton == nil {
            doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(editAction(_:)))
        }
        
        if editButton == nil {
            editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editAction(_:)))
        }
        
        navigationItem.leftBarButtonItem = (
            tableView.isEditing ? doneButton : editButton
        )
    }
    func editAction(_ sender: Any?) {
        if tableView.isEditing {
            tableView.setEditing(false, animated: true)
        } else {
            tableView.setEditing(true, animated: true)
        }
        refreshLeftButton()
    }
    
    var tableServers: [Server] = []
    func reload() {
        tableServers = ServerListModel.shared.readState().1
        tableView.reloadData()
    }
    
    func addAction(_ sender: Any?) {
        let nc: UINavigationController = self.makeVC("AddServerNC")
        let vc: AddServerVC = self.makeVC("AddServerVC")
        vc.tvc = self
        nc.pushViewController(vc, animated: false)
        self.present(nc, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableServers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServerCell", for: indexPath)
        let server = tableServers[indexPath.row]
        var text = server["url"]!
        if let name = ServerMetadataCache.shared.getValue(serverUUID: server["uuid"]!, key: "name") {
            if !name.isEmpty {
                text = name
            }
        }
        cell.textLabel?.text = text
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let server = tableServers[indexPath.row]
        self.openServer(server)
    }
    func openServer(_ server: Server) {
        let url = server["url"]!
        let vc: MainVC = self.makeVC("MainVC")
        vc.tvc = self
        vc.setup(server: server, url: url)
        self.navigationController!.pushViewController(vc, animated: false)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            ServerListModel.shared.deleteServer(index: indexPath.row)
            self.reload()
//            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
//        tableView.moveRow(at: fromIndexPath, to: to)
//        ServerListModel.shared.swapServers(fromIndexPath.row, to.row)
//        self.reload()
        // this doesn't work properly
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

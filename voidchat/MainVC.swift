//  Created by Alex Gordon on 24/07/2018.
//  Copyright © 2018 Alex Gordon. See LICENSE.txt

import UIKit
import WebKit

class MainVC: UIViewController, WKUIDelegate, WKNavigationDelegate, UITextViewDelegate, WKScriptMessageHandler {
    @IBOutlet var containerView: UIView!;
    @IBOutlet var innerView: UIView!;
    @IBOutlet var inputTV: MPTextView!;
    @IBOutlet var containerBottomConstraint: NSLayoutConstraint!;
    var web: WKWebView!;
    
    var savedURL: String?
    var savedServer: Server?
    var serverName = ""
    var serverUUID = ""
    weak var tvc: ServersTVC? = nil
    
    @IBAction func sendMessage(_ sender: Any?) {
        let dict = ["message": self.inputTV.text!];
        let jsonData = try! JSONSerialization.data(
            withJSONObject: dict,
            options: []
        );
        let jsonString = String(data: jsonData, encoding: .utf8)!;
        self.web.evaluateJavaScript("window.vcSendButton(\(jsonString))") { (val, err) in
        };
    }
    @IBAction func atButton(_ sender: Any?) { self.inputTV.insertText("@"); }
    @IBAction func slashButton(_ sender: Any?) { self.inputTV.insertText("/"); }
    @IBAction func hashButton(_ sender: Any?) { self.inputTV.insertText("#"); }
    @IBAction func boldButton(_ sender: Any?) { self.inputTV.insertText("*"); }
    @IBAction func italicButton(_ sender: Any?) { self.inputTV.insertText("_"); }
    @IBAction func codeButton(_ sender: Any?) { self.inputTV.insertText("`"); }
    
    @IBAction func fileAttachmentButton(_ sender: Any?) {
        // TODO
    }
    @IBAction func imageAttachmentButton(_ sender: Any?) {
    // TODO
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        let resp = navigationResponse.response as! HTTPURLResponse
        let supported = (resp.allHeaderFields["x-vc-app-supported"] as? String) ?? "no"
        if supported != "yes" {
            // This is required to meet Apple's guidelines
            return decisionHandler(.cancel)
        }
        
        // Extract the server name and set it on the titlebar
        let name = (resp.allHeaderFields["x-vc-server-name"] as? String) ?? ""
        if self.serverName != name, !name.isEmpty {
            self.serverName = name
            serverNameDidChange()
        }
        decisionHandler(.allow)
    }
    func serverNameDidChange() {
        let uuid = self.serverUUID
        self.title = self.serverName
        ServerMetadataCache.shared.setValue(serverUUID: uuid, key: "name", value: self.serverName)
        self.tvc?.reload()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let url = navigationAction.request.url {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
                decisionHandler(.cancel)
            } else {
                decisionHandler(.allow)
            }
        } else {
            decisionHandler(.allow)
        }
    }
    
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        // -1 = never show
        // 0 = show on mention (default)
        // 1 = show always
        guard let body = message.body as? NSDictionary else { return; }
        guard let messageType = body["kind"] as? NSString else { return; }
        switch messageType {
            case "makeFocused":
                self.inputTV.becomeFirstResponder();
            case "hideInputBar":
                self.setInputBarHidden(true);
            case "showInputBar":
                self.setInputBarHidden(false);
            case "setSelection":
                guard let start = body["start"] as? NSNumber else { return; }
                guard let end = body["end"] as? NSNumber else { return; }
                let s = start.intValue;
                let e = end.intValue;
                /*
                let tv = self.inputTV;
                let pA = tv?.beginningOfDocument;
                let pZ = tv?.endOfDocument;
                let rAZ = tv?.textRange(from: pA!, to: pZ!);
                let qA = tv?.position(within: rAZ!, atCharacterOffset: start.intValue);
                let qZ = tv?.position(within: rAZ!, atCharacterOffset: end.intValue);
                let sAZ = tv?.textRange(from: qA!, to: qZ!);
                self.inputTV.selectedTextRange = sAZ;
                */
                
                let n = self.inputTV.textStorage.length;
                if s < 0 { return; }
                if s > n { return; }
                if e < 0 { return; }
                if e > n { return; }
                if s > e { return; }
                self.inputTV.selectedRange = NSMakeRange(s, e - s);
            case "setText":
                guard let text = body["text"] as? NSString else { return; }
                self.inputTV.text = text as String;
                self.setSendEnabled()
            case "setPlaceholder":
                guard let text = body["text"] as? NSString else { return; }
                self.inputTV.placeholderText = text as String;
            default:
                return;
            
        }
        
        /*
        let notifLevel = UserDefaults.standard.integer(forKey: "VCNotificationLevel");
        if (notifLevel == -1) { return; }
        
        guard let body = message.body as? NSDictionary else { return; }
        guard let serverName = body["serverName"] as? NSString else { return; }
        // guard let roomId = body["roomId"] as? NSNumber else { return; }
        guard let roomName = body["roomName"] as? NSString else { return; }
        guard let kind = body["kind"] as? NSString else { return; }
        guard let isMention = body["isMention"] as? NSNumber else { return; }
        guard let message = body["message"] as? NSString else { return; }
        // guard let id = body["id"] as? NSString else { return; }
        guard let senderUsername = body["senderUsername"] as? NSString else { return; }
        // guard let senderDisplayName = body["senderDisplayName"] as? NSString else { return; }
        
        if (kind != "message") { return; }
        if (!isMention.boolValue && notifLevel == 0) { return; }
        
        let unc = NSUserNotificationCenter.default;
        unc.delegate = self;
        let un = NSUserNotification();
        un.title = "voidchat (\(serverName))";
        un.subtitle = "\(roomName)"; // TODO
        un.informativeText = "\(senderUsername): \(message)";
        un.hasReplyButton = true;
        un.responsePlaceholder = "Message @\(senderUsername)";
        unc.deliver(un);
        */
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let dict = ["message": self.inputTV.text!];
        let jsonData = try! JSONSerialization.data(
            withJSONObject: dict,
            options: .init(rawValue: 0)
        );
        let jsonString = String(data: jsonData, encoding: .utf8)!;
        self.web.evaluateJavaScript("window.vcInputChanged(\(jsonString))", completionHandler: nil);
        
        self.setSendEnabled()
    }
    func textViewDidChangeSelection(_ textView: UITextView) {
        let sr = self.inputTV.selectedRange;
        let start = sr.location;
        let end = NSMaxRange(sr);
        self.web.evaluateJavaScript("window.vcSelectionChanged(\(start), \(end))", completionHandler: nil);
    }
    
    @IBOutlet var kbav: UIView!
    var kbavHidingConstraint: NSLayoutConstraint? = nil
    func setInputBarHidden(_ hidden: Bool) {
        if kbav.isHidden == hidden { return }
        kbav.isHidden = hidden;
        if hidden {
            if kbavHidingConstraint == nil {
                kbavHidingConstraint = NSLayoutConstraint(
                    item: kbav,
                    attribute: .height,
                    relatedBy: .equal,
                    toItem: nil,
                    attribute: .notAnAttribute,
                    multiplier: 1.0,
                    constant: 0)
                kbavHidingConstraint?.priority = UILayoutPriorityRequired
            }
            kbav.superview?.addConstraint(kbavHidingConstraint!)
        } else {
            kbav.superview?.removeConstraint(kbavHidingConstraint!)
        }
    }
    
    @IBOutlet var sendButton: UIButton!;
    private func setSendEnabled() {
        let b = self.inputTV.textStorage.length > 0;
        if self.sendButton.isEnabled == b { return }
        
        self.sendButton.isEnabled = b;
        self.sendButton.backgroundColor = b ? #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1) : #colorLiteral(red: 0.9, green: 0.9, blue: 0.9, alpha: 1);
        self.sendButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        self.sendButton.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: .disabled)
        self.sendButton.setTitleColor(#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1), for: .highlighted)
        self.sendButton.tintColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1);
        //self.sendButton.tintColor = UIColor.white;
    }
    
    // TODO:
    // - command tray doesn't show up
    // - bidirection msginput communication so that when we set value on #MessageInput it also changes the app's text field
    // - roomview queries the selectionStart and selectionEnd of msginput
    // -- really what we need is a MessageInputProxy _class_ that handles all of this
    // -- get/set value on MessageInputProxy does what it should
    // -- get/set selectionStart and selectionEnd
    // -- etc
    // -- commands are case sensitive
    // - fix scrolling so that it listens for scroll events on window
    // [client] linkcontroller
    // [app] notifications
    // [app] reachability events
    // [app] add : and # buttons
    // [app] add some separator lines
    // [???] text was clipped
    
    
    /*
    override func webView(
        _ webView: WKWebView,
        runOpenPanelWith parameters: WKOpenPanelParameters,
        initiatedByFrame frame: WKFrameInfo,
        completionHandler: @escaping ([URL]?) -> Void
    ) {
        
    }
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inputTV.placeholderText = "";
        self.inputTV.delegate = self;
        self.setSendEnabled();
        
        let view = WKWebView(frame: CGRect(x: 0, y: 0, width: 350, height: 350))
        view.customUserAgent = "vciosapp";
        view.configuration.userContentController.add(self, name: "vcInputDelegate");
        view.navigationDelegate = self
        view.uiDelegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        self.web = view;
        self.innerView.addSubview(view)
        
        NSLayoutConstraint.activate([
            // self.topLayoutGuide.bottomAnchor.constraint(equalTo: web.topAnchor),
            // self.bottomLayoutGuide.topAnchor.constraint(equalTo: web.bottomAnchor),
            innerView.topAnchor.constraint(equalTo: web.topAnchor),
            innerView.bottomAnchor.constraint(equalTo: web.bottomAnchor),
            
            innerView.leadingAnchor.constraint(equalTo: web.leadingAnchor),
            innerView.trailingAnchor.constraint(equalTo: web.trailingAnchor),
        ])
        view.scrollView.contentInset = UIEdgeInsets.zero
        
        self.subscribeTo(name: .UIKeyboardWillShow, selector: #selector(keyboardWillShow(_:)))
        self.subscribeTo(name: .UIKeyboardDidShow, selector: #selector(keyboardDidShow(_:)))
        self.subscribeTo(name: .UIKeyboardWillHide, selector: #selector(keyboardWillHide(_:)))
        
        if let savedURL = self.savedURL,
           let savedServer = self.savedServer {
            self.setup(server: savedServer, url: savedURL)
        }
    }
    func subscribeTo(name: NSNotification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: name, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.web.scrollView.contentInset = UIEdgeInsets.zero
        self.web.scrollView.contentOffset = CGPoint.zero
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func setup(server: Server, url: String) {
        let earl = URL(string: url)!
        
        self.serverUUID = server["uuid"]!
        if let name = ServerMetadataCache.shared.getValue(serverUUID: server["uuid"]!, key: "name"), !name.isEmpty {
            self.title = name
            self.serverName = name
        } else {
            self.title = server["host"] ?? "unknown host"
        }
        
        if self.web == nil {
            self.savedURL = url
            self.savedServer = server
        } else {
            print("Attempting to load: \(earl.absoluteString)")
            web.load(URLRequest(url: earl))
            self.savedURL = nil
            self.savedServer = nil
        }
    }
    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
    
    func keyboardDidShow(_ note: Notification?) {
        let userInfo = note?.userInfo
//        let duration = TimeInterval((userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double)!)
//        let curve = UIViewAnimationCurve(rawValue: (userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? Int)!)!
        var keyboardFrameEnd: CGRect = (userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)!.cgRectValue
        keyboardFrameEnd = view.convert(keyboardFrameEnd, from: nil)
//        let raw1 = UIViewAnimationOptions.beginFromCurrentState.rawValue;
//        let raw2 = UInt(curve.rawValue);
//        let raw = raw1 | raw2;
        print("kb did show end frame \(keyboardFrameEnd)")
    }
    func keyboardWillShow(_ note: Notification?) {
        let userInfo = note?.userInfo
        let duration = TimeInterval((userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double)!)
        let curve = UIViewAnimationCurve(rawValue: (userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? Int)!)!
        var keyboardFrameEnd: CGRect = (userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)!.cgRectValue
        keyboardFrameEnd = view.convert(keyboardFrameEnd, from: nil)
        let raw1 = UIViewAnimationOptions.beginFromCurrentState.rawValue;
        let raw2 = UInt(curve.rawValue);
        let raw = raw1 | raw2;
        print("kb end frame \(keyboardFrameEnd)")
        let viewHeight = view.frame.size.height
        UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions(rawValue: raw), animations: {
            //self.innerView.frame = CGRect(x: 0, y: 0, width: keyboardFrameEnd.size.width, height: keyboardFrameEnd.origin.y)
            //self.web!.scrollView.bounds.origin.y +=
            self.containerBottomConstraint.constant = viewHeight - keyboardFrameEnd.origin.y; //keyboardFrameEnd.origin.y; // keyboardFrameEnd.size.height;
            // self.containerView.frame = CGRect(x: 0, y: 0, width: keyboardFrameEnd.size.width, height: keyboardFrameEnd.origin.y)
        })
    }
    func keyboardWillHide(_ note: Notification?) {
        let userInfo = note?.userInfo
        let duration = TimeInterval((userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double)!)
        let curve = UIViewAnimationCurve(rawValue: (userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? Int)!)!
        var keyboardFrameEnd: CGRect = (userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)!.cgRectValue
        keyboardFrameEnd = view.convert(keyboardFrameEnd, from: nil)
        let raw1 = UIViewAnimationOptions.beginFromCurrentState.rawValue;
        let raw2 = UInt(curve.rawValue);
        let raw = raw1 | raw2;
        UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions(rawValue: raw), animations: {
            self.containerBottomConstraint.constant = 0; // keyboardFrameEnd.size.height;
            //self.containerView.frame = CGRect(x: 0, y: 0, width: keyboardFrameEnd.size.width, height: keyboardFrameEnd.origin.y)
        })
    }
}


//  Created by Alex Gordon on 14/08/2018.
//  Copyright © 2018 Alex Gordon. See LICENSE.txt

import UIKit

enum SomeError: Error {
    case invalidURL
}

class AddServerVC: UIViewController {
    
    @IBOutlet var connectButton: UIButton!;
    @IBOutlet var textField: UITextField!;
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction(_:)))
        navigationItem.rightBarButtonItem = cancelButton
        
        textField.becomeFirstResponder()
    }
    var firstLaunch = false
    weak var tvc: ServersTVC?
    
    @IBAction func connect(_ sender: Any?) {
        do {
            var host = self.textField.text ?? "";
            let trimmable = CharacterSet.whitespacesAndNewlines.union(CharacterSet(charactersIn: "/:."))
            host = host.trimmingCharacters(in: trimmable)
            if host.contains("://") {
                // Don't strip http://
            } else {
                host = "https://" + host
            }
            
            guard var url = URL(string: host) else { throw SomeError.invalidURL }
            guard var comps = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
                throw SomeError.invalidURL
            }
            guard let hostname = comps.host else { throw SomeError.invalidURL }
            if hostname.isEmpty { throw SomeError.invalidURL }
            
            comps.query = nil
            comps.fragment = nil
            
            if comps.host == "localhost" || comps.host == "127.0.0.1" {
                comps.scheme = "http"
            } else {
                comps.scheme = "https"
            }
            
            let path = comps.path
            if !path.hasSuffix("/") {
                comps.path = path + "/"
            }
            
            if comps.url == nil { throw SomeError.invalidURL } else { url = comps.url! }
            
            let newServer = [
                "url": url.absoluteString,
                "uuid": makeUUID(),
                "hostname": hostname,
                "host": comps.port == nil ? hostname : "\(hostname):\(comps.port!)",
            ]
            let _ = ServerListModel.shared.addServer(newServer: newServer)
            
            tvc?.reload()
            self.navigationController!.dismiss(animated: true, completion: {
                self.tvc?.openServer(newServer)
            })
        } catch {
            let alert = UIAlertController(title: "Invalid URL", message: "The given URL is not valid.", preferredStyle: .alert);
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func textField(_ sender: Any?) {
    }
    
    @IBAction func cancelAction(_ sender: Any?) {
        self.navigationController!.dismiss(animated: true, completion: nil)
    }
}

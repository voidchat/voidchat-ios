//  Created by Alex Gordon on 08/08/2018.
//  Copyright © 2018 Alex Gordon. See LICENSE.txt

import UIKit

class TopBorderView: UIView {
    override func draw(_ rect: CGRect) {
        var borderRect = self.bounds;
        borderRect.size.height = 1;
        let ctx = UIGraphicsGetCurrentContext()!;
        let color = 0.90 as CGFloat;
        ctx.setFillColor(red: color, green: color, blue: color, alpha: 1.0);
        ctx.fill(borderRect);
    }
}

//  Created by Alex Gordon on 19/09/2018.
//  Copyright © 2018 Alex Gordon. See LICENSE.txt

import UIKit

class ServersNC: UINavigationController, UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let ipgr = self.navigationController?.interactivePopGestureRecognizer {
            ipgr.delegate = self
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
}

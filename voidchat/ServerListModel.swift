//  Created by Alex Gordon on 19/09/2018.
//  Copyright © 2018 Alex Gordon. See LICENSE.txt

import UIKit

typealias Server = [String: String]
typealias ServerUUID = String

func makeUUID() -> ServerUUID {
    return UUID().uuidString
}

class ServerMetadataCache {
    static let shared = ServerMetadataCache()
    
    private var dirty = false
    var cachePath: URL? = nil
    var cache = [String: String]()
    init() {
        do {
            let cacheDir = try FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            cachePath = cacheDir.appendingPathComponent("vccache.json")
            
            let data = try Data(contentsOf: cachePath!)
            if let cache = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String] {
                self.cache = cache
            }
        } catch {
        }
    }
    func setValue(serverUUID: ServerUUID, key: String, value: String) {
        cache["\(serverUUID)_\(key)"] = value
        setDirty()
    }
    func getValue(serverUUID: ServerUUID, key: String) -> String? {
        return cache["\(serverUUID)_\(key)"]
    }
    func setDirty() {
        if dirty { return }
        dirty = true
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (timer) in
            self.dirty = false
            do {
                guard let cachePath = self.cachePath else { return }
                let data = try JSONSerialization.data(withJSONObject: self.cache, options: [])
                try data.write(to: cachePath, options: .atomic)
            } catch {
            }
        }
    }
}

class ServerListModel {
    static let shared = ServerListModel()
    
    func readState() -> (ServerUUID?, [Server], [ServerUUID:Server]) {
        guard let serversData = UserDefaults.standard.data(forKey: "vcServers") else { return (nil, [], [:]) }
        guard let serversA = try? JSONSerialization.jsonObject(with: serversData, options: []) else { return (nil, [], [:]) }
        guard let serversB = serversA as? [[String: String]] else { return (nil, [], [:]) }
        var lastUsed = UserDefaults.standard.string(forKey: "vcLastUsedServerUUID")
        var serverMap = [ServerUUID:Server]()
        for server in serversB { serverMap[server["uuid"]!] = server }
        if lastUsed != nil && serverMap[lastUsed!] == nil {
            lastUsed = nil
        }
        return (lastUsed, serversB, serverMap)
    }
    func setLastUsedServer(_ i: ServerUUID?) {
        UserDefaults.standard.set(i, forKey: "vcLastUsedServerUUID")
    }
    func lastUsedServer() -> ServerUUID? {
        return UserDefaults.standard.string(forKey: "vcLastUsedServerUUID")
    }
    func deleteServer(index i: Int) {
        var (lastUsedServer, servers, _) = self.readState()
        if i < 0 { return }
        if i >= servers.count { return }
        
        if lastUsedServer == servers[i]["uuid"]! {
            lastUsedServer = nil
        }
        servers.remove(at: i)
        self.writeState(servers)
        self.setLastUsedServer(lastUsedServer)
    }
    func swapServers(_ i: Int, _ j: Int) {
        var (_, servers, _) = self.readState()
        if i == j { return }
        if i < 0 || i >= servers.count { return }
        if j < 0 || j >= servers.count { return }
        
        (servers[j], servers[i]) = (servers[i], servers[j])
        
        self.writeState(servers)
    }
    func replaceServer(index i: Int, server: Server) {
        var (_, servers, _) = self.readState()
        if i < 0 { return }
        if i >= servers.count { return }
        
        servers[i] = server
        
        self.writeState(servers)
    }
    func serverAt(index i: Int) -> Server? {
        var (_, servers, _) = self.readState()
        if i < 0 { return nil }
        if i >= servers.count { return nil }
        return servers[i]
    }
    func addServer(newServer: Server) -> Int? {
        var (_, servers, _) = readState();
        for (i, existingServer) in servers.enumerated() {
            if existingServer["url"]! == newServer["url"]! {
                return i
            }
        }
        if servers.isEmpty {
            setLastUsedServer(newServer["uuid"]!);
        }
        servers.append(newServer)
        self.writeState(servers)
        return nil
    }
    func writeState(_ servers: [Server]) {
        let json = try! JSONSerialization.data(withJSONObject: servers, options: []);
        UserDefaults.standard.set(json, forKey: "vcServers")
    }
}
